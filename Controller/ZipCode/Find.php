<?php
namespace Avanti\StorePickup\Controller\ZipCode;

use Avanti\RedirectByGeoip\Model\ZipcodeTrack;
use Exception;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Avanti\StorePickup\Block\Product\View\StorePickup as StorePickupBlock;
use FME\GoogleMapsStoreLocator\Model\ResourceModel\Storelocator\CollectionFactory as StoreLocatorCollection;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;

class Find extends Action
{
    /**
     * @var ZipcodeTrack
     */
    protected $zipcodeTrack;

    /**
     * @var WebsiteRepositoryInterface
     */
    protected $websiteRepositoryInterface;

    /**
     * @var StoreRepositoryInterface
     */
    protected $storeRepositoryInterface;

    /**
     * @var StorePickupBlock
     */
    protected $storePickupBlock;

    /**
     * @var StoreLocatorCollection
     */
    protected $storeLocatorCollection;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var ProductRepositoryInterface
     */
    protected  $productRepositoryInterface;

    /**
     * Find constructor.
     * @param ZipcodeTrack $zipcodeTrack
     * @param WebsiteRepositoryInterface $websiteRepositoryInterface
     * @param StoreRepositoryInterface $storeRepositoryInterface
     * @param StorePickupBlock $storePickupBlock
     * @param StoreLocatorCollection $storeLocationCollection
     * @param ScopeConfigInterface $scopeConfig
     * @param ProductRepositoryInterface $productRepositoryInterface
     * @param Context $context
     */
    public function __construct(
        ZipcodeTrack $zipcodeTrack,
        WebsiteRepositoryInterface $websiteRepositoryInterface,
        StoreRepositoryInterface $storeRepositoryInterface,
        StorePickupBlock $storePickupBlock,
        StoreLocatorCollection $storeLocatorCollection,
        ScopeConfigInterface $scopeConfig,
        ProductRepositoryInterface $productRepositoryInterface,
        Context $context
    ) {
        $this->zipcodeTrack = $zipcodeTrack;
        $this->websiteRepositoryInterface = $websiteRepositoryInterface;
        $this->storeRepositoryInterface = $storeRepositoryInterface;
        $this->storePickupBlock = $storePickupBlock;
        $this->storeLocatorCollection = $storeLocatorCollection;
        $this->scopeConfig = $scopeConfig;
        $this->productRepositoryInterface = $productRepositoryInterface;
        parent::__construct($context);
    }

    public function execute()
    {
        $zipcode = $this->getRequest()->getParam('zipcode') ?? null;
        $productId = $this->getRequest()->getParam('product_id') ?? null;

        if (!$zipcode || !$productId) {
            return $this->resultFactory
                ->create(ResultFactory::TYPE_JSON)
                ->setData([
                    'success' => false,
                    'error_message' => __('Parameter zipcode and product_id is required')
                ]);
        }
        try {
            $storeData = $this->zipcodeTrack->getFromZipcode($zipcode);
            if (isset($storeData[0])) {
                $websiteCodeZipCode = $storeData[0]->getData('website_code');
                $websiteZipCode = $this->websiteRepositoryInterface->get($websiteCodeZipCode);

                $product = $this->productRepositoryInterface->getById($productId);
                $storeIds = $product->getStoreIds();

                $found = false;
                $websiteCode = 0;
                $storeIdFound = 0;
                foreach ($storeIds as $storeId) {
                    $website = $this->storePickupBlock->getWebsiteCode($storeId);
                    if ($websiteCodeZipCode == $website) {
                        $found = true;
                        $websiteCode = $website;
                        $storeIdFound = $storeId;
                        break;
                    }
                }

                if ($found && $storeIdFound !=0) {
                    $storePickupEnable = $this->scopeConfig->getValue("carriers/avanti_storepickup/active",
                        ScopeInterface::SCOPE_WEBSITE, $websiteCode);

                    if ($storePickupEnable) {
                        $collection = $this->storeLocatorCollection->create();
                        $storesFme = $collection->addFieldToFilter('is_active', 1)->addFieldToFilter('store_id', $storeIdFound)->setOrder('city', 'ASC')->getData();

                        if (count($storesFme) > 0) {
                            return $this->resultFactory
                                ->create(ResultFactory::TYPE_JSON)
                                ->setData([
                                    'success' => true,
                                    'store' => $storesFme
                                ]);
                        }
                    }

                    return $this->resultFactory
                        ->create(ResultFactory::TYPE_JSON)
                        ->setData([
                            'success' => false,
                            'error_message' => __("No Stores Found")
                        ]);
                }
            }
        } catch (Exception $e) {
            return $this->resultFactory
                ->create(ResultFactory::TYPE_JSON)
                ->setData([
                    'success' => false,
                    'error_message' => $e->getMessage()
                ]);
        }
        return $this->resultFactory
            ->create(ResultFactory::TYPE_JSON)
            ->setData([
                'success' => false,
                'error_message' => __('No Stores Found')
            ]);
    }
}