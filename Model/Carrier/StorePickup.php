<?php
declare(strict_types=1);

namespace Avanti\StorePickup\Model\Carrier;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Rate\ResultFactory;
use Psr\Log\LoggerInterface;
use Magento\Store\Model\ScopeInterface;

class StorePickup extends AbstractCarrier implements CarrierInterface
{
    protected $_code = 'avanti_storepickup';
    protected $_isFixed = true;

    protected $rateResultFactory;
    protected $rateMethodFactory;

    /**
     * Constructor
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param ErrorFactory $rateErrorFactory
     * @param LoggerInterface $logger
     * @param ResultFactory $rateResultFactory
     * @param MethodFactory $rateMethodFactory
     * @param array $data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $rateResultFactory,
        MethodFactory $rateMethodFactory,
        array $data = []
    ) {
        $this->rateResultFactory = $rateResultFactory;
        $this->rateMethodFactory = $rateMethodFactory;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->moduleActive()) {
            return false;
        }

        $shippingPrice = $this->getCarrierPrice();
        $result = $this->rateResultFactory->create();

        if ($shippingPrice !== false) {
            $method = $this->rateMethodFactory->create();

            $method->setCarrier($this->_code);
            $method->setCarrierTitle($this->getCarrierTitle());

            $method->setMethod($this->_code);
            $method->setMethodTitle($this->getCarrierMame());

            $method->setPrice($shippingPrice);
            $method->setCost($shippingPrice);

            $result->append($method);
        }

        return $result;
    }

    /**
     * getAllowedMethods
     *
     * @return array
     */
    public function getAllowedMethods()
    {
        return [$this->_code => $this->getConfigData('name')];
    }

    private function moduleActive()
    {
        return $this->getConfig('carriers/avanti_storepickup/active');
    }

    private function getCarrierPrice()
    {
        return $this->getConfig('carriers/avanti_storepickup/price');
    }

    private function getCarrierTitle()
    {
        return $this->getConfig('carriers/avanti_storepickup/title');
    }

    private function getCarrierMame()
    {
        return $this->getConfig('carriers/avanti_storepickup/name');
    }

    private function getConfig($path)
    {
        $webSiteScope = ScopeInterface::SCOPE_WEBSITE;
        return $this->_scopeConfig->getValue($path, $webSiteScope);
    }


}
