<?php

namespace Avanti\StorePickup\Block\Product\View;

use Magento\Framework\View\Element\Template;
use \Magento\Framework\Registry;
use Magento\Framework\Pricing\Helper\Data as PriceHelper;
use Magento\Framework\View\Element\Template\Context;
use FME\GoogleMapsStoreLocator\Model\ResourceModel\Storelocator\CollectionFactory as StoreLocatorCollection;
use Magento\Directory\Model\RegionFactory;
use Avanti\WebsiteSwitcher\Helper\Config as WebsiteSwitcherHelper;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Magento\Catalog\Helper\Image as ImageHelper;

class StorePickup extends Template
{
    const DISCOUNT_CASH = 5;

    protected $registry;
    protected $priceHelper;
    protected $storeLocatorCollection;
    protected $regionFactory;
    protected $websiteSwitcherHelper;
    protected $storeRepositoryInterface;
    protected $websiteRepositoryInterface;
    protected $imageHelper;

    public function __construct(
        Registry $registry,
        Context $context,
        PriceHelper $priceHelper,
        StoreLocatorCollection $storeLocatorCollection,
        RegionFactory $regionFactory,
        WebsiteSwitcherHelper $websiteSwitcherHelper,
        StoreRepositoryInterface $storeRepositoryInterface,
        WebsiteRepositoryInterface $websiteRepositoryInterface,
        ImageHelper $imageHelper,
        array $data = [])
    {
        $this->registry = $registry;
        $this->priceHelper = $priceHelper;
        $this->storeLocatorCollection = $storeLocatorCollection;
        $this->regionFactory = $regionFactory;
        $this->websiteSwitcherHelper = $websiteSwitcherHelper;
        $this->storeRepositoryInterface = $storeRepositoryInterface;
        $this->websiteRepositoryInterface = $websiteRepositoryInterface;
        $this->imageHelper = $imageHelper;
        parent::__construct($context, $data);
    }

    public function getCurrentProduct()
    {
        return $this->registry->registry('current_product');
    }

    public function getProductInfo() :array
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $this->getCurrentProduct();
        if($product) {
            $productData = [
                "product_name" => $product->getName(),
                "product_normal_price" => $this->priceHelper->currency($product->getPrice(),true,false),
                'product_discount_price' => $this->discountInChash($product->getPrice()),
            ];
            $url = $this->imageHelper->init($product, 'product_small_image')->getUrl();
            if ($url) {
                $productData['product_image'] = $url;
            }
            $storeIds = $product->getStoreIds();

            $infos = $this->getStoreInfo($storeIds);
            if (is_array($infos)) {
                $productData['stores'] = $infos;
            }
            return $productData;
        }
        return [];
    }

    /**
     * @param $price
     * @return float|string
     */
    private function discountInChash($price)
    {
        $discountPrice = $price - ((self::DISCOUNT_CASH * $price ) / 100);
        if ($discountPrice < 1000) {
            $discountPriceFinal = $this->priceHelper->currency(number_format($discountPrice,2),true,false);
        } else {
            $discountPriceFinal = $this->priceHelper->currency($discountPrice,true,false);
        }

        return $discountPriceFinal;
    }

    public function getStoreInfo($storeIds)
    {
        foreach ($storeIds as $key => $value) {
            $websiteCode = $this->getWebsiteCode($value);
            $enable = $this->_scopeConfig->getValue("carriers/avanti_storepickup/active", \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE, $websiteCode);
            if (!$enable) {
                unset($storeIds[$key]);
            }
        }
        $storeIds = implode(",",$storeIds);

        $collection = $this->storeLocatorCollection->create();
        return $collection->addFieldToFilter('is_active', 1)->addFieldToFilter('store_id', ['in' => $storeIds])->setOrder('city', 'ASC')->getData();
    }

    public function getFullAddress($store)
    {
        return $store['address_street'] . ', ' . $store['address_number'] .
            ' - ' . $store['address_neighborhood'] . ', ' .
            $store['city'] . ' - ' . $this->getRegion($store['region_id']) .
            ' - ' . $store['zipcode'];
    }

    public function getRegion($regionId)
    {
        $region = $this->regionFactory->create();
        $region->load($regionId);
        return $region->getData('code');
    }

    public function getWebsiteChangeUrl($storeId)
    {
        $websiteCode = $this->getWebsiteCode($storeId);
        $url =  $this->getUrl(WebsiteSwitcherHelper::WEBSITESWITCHER_WEBSITE_CHANGEACTION, ['_secure' => true]);
        $url .= 'website/'.$websiteCode;
        return $url;
    }

    public function getWebsiteCode($storeId)
    {
        $store = $this->storeRepositoryInterface->get($storeId);
        $websiteId = $store->getWebsiteId();
        $website = $this->websiteRepositoryInterface->get($websiteId);
        return $website->getCode();
    }
}